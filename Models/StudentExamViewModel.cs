﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCRUD.Models
{
    public class StudentExamViewModel
    {
        public int StudentId { get; set; }

        public int ExamId { get; set; }

        public string Note { get; set; }
        public IEnumerable<SelectListItem> Students { get; set; }
        public IEnumerable<SelectListItem> Exams { get; set; }
    }
}
