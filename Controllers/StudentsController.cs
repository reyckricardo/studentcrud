﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StudentCRUD.Data;
using StudentCRUD.Data.Entities;
using StudentCRUD.Models;

namespace StudentCRUD.Controllers
{
    public class StudentsController : Controller
    {
        private readonly IRepository repository;

        public StudentsController(IRepository repository)
        {
            this.repository = repository;
        }

        // GET: Students
        public IActionResult Index()
        {
            return View(this.repository.GetStudents());
        }

        // GET: Students/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

             var student = this.repository.GetStudent(id.Value);

            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Students/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( Student student)
        {

            
            if (ModelState.IsValid)
            {
                this.repository.AddStudent(student);
                this.repository.SaveAllAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Students/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = this.repository.GetStudent(id.Value); 
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,Student student)
        {
            if (id != student.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    this.repository.UpdateStudent(student);
                    this.repository.SaveAllAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (this.repository.StudentExists(student.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Students/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = this.repository.GetStudent(id.Value);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = this.repository.GetStudent(id);
            this.repository.RemoveStudent(student);
            this.repository.SaveAllAsync();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult AddStudentExam() {

            var model = new StudentExamViewModel
            {
                Exams = this.GetProductsList(),
                Students = this.GetProductsListStudent()

            };
            return View(model);
        }



        private IEnumerable<SelectListItem> GetProductsList()
        {
            var products = this.repository.GetExams().ToList();
            products.Insert(0, new Exam
            {
                Id = 0,
                Name = "(Select a Student...)"
            });

            return products.Select(p => new SelectListItem
            {
                Value = p.Id.ToString(),
                Text = p.Name
            }).ToList();
        }

 


        private IEnumerable<SelectListItem> GetProductsListStudent()
        {
            var s = this.repository.GetStudents().ToList();
            s.Insert(0, new Student
            {
                Id = 0,
                Name = "(Select a Student...)"
            });

            return s.Select(p => new SelectListItem
            {
                Value = p.Id.ToString(),
                Text = p.Name
            }).ToList();
        }
        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddStudentExam(StudentExamViewModel Examstudent)
        {  

            if (ModelState.IsValid)
            {

                var studentW = this.repository.GetStudent(Examstudent.StudentId);
                var examw = this.repository.GetExam(Examstudent.ExamId);

                var studentExam = new StudentExam();


                studentExam.Student = studentW;
                studentExam.Exam = examw;
               
                this.repository.AddStudentExam(studentExam);
                this.repository.SaveAllAsync();
                return RedirectToAction(nameof(Index));

                 
                //return RedirectToAction(nameof(Index));
            }
            return View(Examstudent);
        }


    }
}
