﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StudentCRUD.Data;
using StudentCRUD.Data.Entities;
using StudentCRUD.Models;

namespace StudentCRUD.Controllers
{
    public class StudentExamsController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IRepository repository;
          
        public StudentExamsController(IRepository repository)
        {
            this.repository = repository;
        }

        // GET: StudentExams
        public  IActionResult Index()
        {
            return View(this.repository.GetStudentExams());
        }

        // GET: StudentExams/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentExam = await _context.StudentExams
                .FirstOrDefaultAsync(m => m.Id == id);
            if (studentExam == null)
            {
                return NotFound();
            }

            return View(studentExam);
        }

        // GET: StudentExams/Create
        public IActionResult Create()
        {
            var model = new StudentExamViewModel
            {
                Exams = this.GetProductsList(),
                Students = this.GetProductsListStudent()

            };
            return View(model);
        }

        // POST: StudentExams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(StudentExamViewModel studentExam)
        {
            if (ModelState.IsValid)
            {

                var studentW = this.repository.GetStudent(studentExam.StudentId);
                var examw = this.repository.GetExam(studentExam.ExamId);

                var studentExamw = new StudentExam();


                studentExamw.Student = studentW;
                studentExamw.Exam = examw;

                this.repository.AddStudentExam(studentExamw);
                this.repository.SaveAllAsync();
                //return RedirectToAction("Create");
                //return RedirectToAction(nameof(Index));
                    

            }
            return View(studentExam);
        }

        // GET: StudentExams/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentExam = await _context.StudentExams.FindAsync(id);
            if (studentExam == null)
            {
                return NotFound();
            }
            return View(studentExam);
        }

        // POST: StudentExams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Note")] StudentExam studentExam)
        {
            if (id != studentExam.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(studentExam);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExamExists(studentExam.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(studentExam);
        }

        // GET: StudentExams/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentExam = await _context.StudentExams
                .FirstOrDefaultAsync(m => m.Id == id);
            if (studentExam == null)
            {
                return NotFound();
            }

            return View(studentExam);
        }

        // POST: StudentExams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var studentExam = await _context.StudentExams.FindAsync(id);
            _context.StudentExams.Remove(studentExam);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentExamExists(int id)
        {
            return _context.StudentExams.Any(e => e.Id == id);
        }


        private IEnumerable<SelectListItem> GetProductsList()
        {
            var products = this.repository.GetExams().ToList();
            products.Insert(0, new Exam
            {
                Id = 0,
                Name = "(Select a Exam...)"
            });

            return products.Select(p => new SelectListItem
            {
                Value = p.Id.ToString(),
                Text = p.Name
            }).ToList();
        }




        private IEnumerable<SelectListItem> GetProductsListStudent()
        {
            var s = this.repository.GetStudents().ToList();
            s.Insert(0, new Student
            {
                Id = 0,
                Name = "(Select a Student...)"
            });

            return s.Select(p => new SelectListItem
            {
                Value = p.Id.ToString(),
                Text = p.Name
            }).ToList();
        }

    }
}
