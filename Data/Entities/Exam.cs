﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCRUD.Data.Entities
{
    public class Exam
    {
        public int Id { get; set; }

        public String Name { get; set; }

        [Range(1, 25)]
        public decimal value { get; set; }

       public IEnumerable<StudentExam> Details { get; set; }
    }
}
