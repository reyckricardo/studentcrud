﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCRUD.Data.Entities
{
    public class Student
    {
        public int Id { get; set; }

        [Required]
        public String Name { get; set; }

        public string Adress { get; set; }

        public int Phone { get; set; }

        public int Age { get; set; }

        public IEnumerable<StudentExam> DetailsExam { get; set; }

    }
}
