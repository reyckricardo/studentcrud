﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCRUD.Data.Entities
{
    public class StudentExam
    {
        public int Id { get; set; }

        public Student Student { get; set; }

        public decimal Note { get; set; }

        public Exam Exam { get; set; }
    }
}
