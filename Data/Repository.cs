﻿using Microsoft.EntityFrameworkCore;
using StudentCRUD.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentCRUD.Data
{
    public class Repository : IRepository
    {
        private readonly ApplicationDbContext context;

        public Repository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Student> GetStudents() {

            return this.context.Students.OrderBy(p => p.Name);

        }
        public Student GetStudent(int id)
        {

            return this.context.Students.Where(p => p.Id == id).FirstOrDefault();

        }
        public Exam GetExam(int id)
        {

            return this.context.Exams.Where(p => p.Id == id).FirstOrDefault();

        }

        public void AddStudent( Student student)
        {
            this.context.Add(student);
        }

        public void AddStudentExam(StudentExam studentExam)
        {
            this.context.Add(studentExam);
        }

        public bool SaveAllAsync()
        {
            return this.context.SaveChanges() > 0;

        }

        public void UpdateStudent(Student student)
        {
            this.context.Update(student);

        }

        public void RemoveStudent(Student student)
        {
            this.context.Remove(student);
        }

        public bool StudentExists(int id)
        {
            return this.context.Students.Any(s => s.Id == id);
        }

        public IEnumerable<Exam> GetExams()
        {
            return this.context.Exams
                
                .Include(e => e.Details)
                .ThenInclude(d => d.Student)
                .OrderBy(e => e.Name);
        }

        public IEnumerable<StudentExam> GetStudentExams()
        {
            return this.context.StudentExams.OrderBy(p => p.Note);
        }
    }
}
