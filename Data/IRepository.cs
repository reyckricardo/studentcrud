﻿using System.Collections.Generic;
using StudentCRUD.Data.Entities;
using StudentCRUD.Models;

namespace StudentCRUD.Data
{
    public interface IRepository
    {
        void AddStudent(Student student);
        void AddStudentExam(StudentExam studentExam); //cambie

        Student GetStudent(int id);
        Exam GetExam(int id);
        IEnumerable <StudentExam> GetStudentExams(); 

        IEnumerable<Student> GetStudents();

        void RemoveStudent(Student student);

        bool SaveAllAsync();

        void UpdateStudent(Student student);

        bool StudentExists(int id);

        IEnumerable<Exam> GetExams();
    }
}